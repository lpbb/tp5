from fastapi import FastAPI
import requests
from client.IsVegan import is_vegan

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/vegan/{code}")
def read_item(code: str):
    return {"code": code, "vegan": is_vegan(code)}



# print(is_vegan("3256540001305"))



