import json
from numpy import void
import requests


def get_api(code : str) :
    x = requests.get('https://world.openfoodfacts.org/api/v2/product/'+str(code))
    return x.json()
