from client.GetApi import get_api

def is_vegan(code : str) -> bool:
    dict_ingredient = get_api(code)
    is_vegan = True
    if 'ingredients' in dict_ingredient['product'].keys():
        for ingredients in dict_ingredient['product']['ingredients'] :
            if 'vegan' in ingredients.keys():
                if ingredients['vegan'] != "yes":
                    is_vegan = False
            else :
                is_vegan = False
    else :
        is_vegan = False
    if is_vegan :
        return True
    else :
        return False




