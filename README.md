# TP 5

Pour lancer l'application :

```
git clone https://gitlab.com/lpbb/tp5.git
cd tp5
pip install -r requirements.txt
uvicorn main:app --reload
```
